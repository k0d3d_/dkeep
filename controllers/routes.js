/**
 * Module dependcies
 */
var
    // jwt = require('jsonwebtoken'),
    crypto = require('crypto'),
    cors = require('cors'),
    appConfig = require('config').express,
    mediaDefaultTTL = require('config').app.mediaDefaultTTL,
    errors = require('../lib/errors');

/**
 * Expose routes
 */

module.exports = function (app, redis_client, jobQueue) {

  var vault = require('./v4ult');
  vault.routes(app, redis_client, jobQueue);

  var cabinet = require('./cabinet');
  cabinet.routes(app, redis_client, jobQueue);


  app.all('/request-token', cors(appConfig.cors.options), function (req, res, next){
    next();
  });



  // should create a new token, store it in redis with a ttl
  // of possible 5mins for prod and 30 - 60 secs for test/dev
  // .return the token back.
  app.post('/request-token', function (req, res, next) {
    // check for token in redisclient,
    // if the token has not expired. the request
    // is passed.
    // if it has expired. a 302 is returned which should force the user
    // to redirect to the download page.
    // also check referrer  and redirect if invalid.

    // this should always be the same as string, that
    // identifies a machine/user .
    // TODO: concat machine id and user logged in as userId value.
    // also with referrer.
    if (!req.get('authorization') ||
        !req.get('referer')) {
      return next(errors.nounce('OperationFailed'));
    }
    var userId = req.get('authorization');
    var token = crypto.randomBytes(32).toString('base64');
    redis_client.set(token, userId + token + req.get('referer'), function (e, r) {
      redis_client.expire(token, mediaDefaultTTL);
      res.send(token);
    });
    // var signed_token = jwt.sign(req.body, token, {expiresInMinutes: 60});
    // //store in redis
    // //send response
    // res.status(200).json(token);
  });

  // //Client Routes
  // var clients = require('./clients');
  // clients.routes(app, auth);

};